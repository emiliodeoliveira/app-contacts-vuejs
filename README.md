
## To do
- Pesquise por rotas. Agora deixe uma tela para o cadastro e uma tela para a listagem.

## Doing
- Pesquise sobre Axios e JSON Server. Realize a persist�ncia dos dados cadastrados (alunos)
- Estude componentes e tente criar a tabela utilizando componentes;

## Done
- Inicie estudando os conceitos b�sicos e as diretivas do Vue.js;
- Realize um cadastro que apenas salve em mem�ria os dados do formul�rio;
- Realize um cadastro que mostre na tabela os dados cadastrados;

